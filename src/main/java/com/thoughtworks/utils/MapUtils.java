package com.thoughtworks.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MapUtils {

    public static Optional<Object> get(Map<String, Object> map, String path) {
        Map<String, Object> subMap = map;
        String[] subPaths = path.split("\\.");
        Object value = null;
        for (String subPath : subPaths) {
            try {
                if (null != value) {
                    subMap = (Map<String, Object>) value;
                }

                String key = pickUpKeyName(subPath);
                if (StringUtils.isEmpty(key)) {
                    return Optional.empty();
                }
                value = subMap.get(key);

                List<Integer> arrayIndexes = pickUpArrayIndex(subPath);
                if (!arrayIndexes.isEmpty()) {
                    for (Integer arrayIndex : arrayIndexes) {
                        value = ((List<Object>) value).get(arrayIndex);
                    }
                }
            } catch (ClassCastException ignored) {
                return Optional.empty();
            }
        }
        return Optional.ofNullable(value);
    }

    private static String pickUpKeyName(String path) {
        return path.split("\\[")[0];
    }

    private static List<Integer> pickUpArrayIndex(String path) {
        Pattern pattern = Pattern.compile("(\\[\\d+\\])+$");
        Matcher matcher = pattern.matcher(path);
        List<Integer> arrayIndexes = new ArrayList<>();
        if (!matcher.find()) {
            return arrayIndexes;
        }

        String indexPath = matcher.group();
        String[] indexes = indexPath.split("]");
        for (String index : indexes) {
            arrayIndexes.add(Integer.parseInt(index.substring(1)));
        }

        return arrayIndexes;
    }

}
