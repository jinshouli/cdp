package com.thoughtworks.service;

import com.thoughtworks.enums.EventType;
import com.thoughtworks.es.EsRepository;
import com.thoughtworks.model.Event;
import com.thoughtworks.model.UpdateDailyMilestoneEventPayload;
import com.thoughtworks.utils.MapUtils;

import java.util.Map;

public class UserProfileService {

    public static Map<String, Object> responseEvent(Event event) {
        if (event.getType() == EventType.UPDATE_DAILY_MILESTONE) {
            return updateDailyMilestone(event);
        }
        return null;
    }

    private static Map<String, Object> updateDailyMilestone(Event event) {
        UpdateDailyMilestoneEventPayload payload =
                new UpdateDailyMilestoneEventPayload((Map<String, Object>) event.getPayload());
        Map<String, Object> record = EsRepository.getUserProfileByCustomerId(payload.getCustomerId());

        Map<String, Object> content = (Map<String, Object>) MapUtils.get(record, "_source").get();
        content.put("dailyMilestone", String.valueOf(payload.getDailyMilestone()));

        return record;
    }

}
