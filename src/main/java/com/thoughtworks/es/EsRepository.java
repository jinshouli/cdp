package com.thoughtworks.es;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.utils.MapUtils;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EsRepository {

    public static final String ES_SERVER = "twdp-dn1:9200";
    public static final String ES_USER_PROFILES_TYPE = "http://twdp-dn1:9200/bmw/user-profiles";
    private static final String QUERY_TEMPLATE = "{\"query\": {\"match\": {\"customerId\": \"%s\" }},\"size\": 1}";

    public static Response get(String url) throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();

        return okHttpClient.newCall(request).execute();
    }

    public static Map<String, Object> getUserProfileByCustomerId(String customerId) {
        String url = ES_USER_PROFILES_TYPE + "/_search";

        OkHttpClient okHttpClient = new OkHttpClient();

        String body = String.format(QUERY_TEMPLATE, customerId);

        MediaType jsonType = MediaType.parse("application/json");
        RequestBody requestBody = RequestBody.create(jsonType, body);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        String bodyString;
        try {
            bodyString = okHttpClient.newCall(request).execute().body().string();
            System.out.println(bodyString);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return obtainResult(bodyString);
    }

    private static Map<String, Object> obtainResult(String bodyString) {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> bodyMap;
        try {
            bodyMap = objectMapper.readValue(bodyString, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        List<Map<String, Object>> hits = (List<Map<String, Object>>) MapUtils
                .get(bodyMap, "hits.hits")
                .orElse(new ArrayList<>());
        if (hits.isEmpty()) {
            return null;
        }

        return hits.get(0);
    }

    public static void updateUserProfile(String id, String bodyString) {
        String url = ES_USER_PROFILES_TYPE + "/" + id;

        OkHttpClient okHttpClient = new OkHttpClient();

        MediaType jsonType = MediaType.parse("application/json");
        RequestBody requestBody = RequestBody.create(jsonType, bodyString);
        Request request = new Request.Builder()
                .url(url)
                .put(requestBody)
                .build();
        try {
            okHttpClient.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
