package com.thoughtworks.enums;

public enum EventType {

    UPDATE_DAILY_MILESTONE,
    UPDATE_LAST_MILESTONE
}
