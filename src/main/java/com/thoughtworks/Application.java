package com.thoughtworks;

import com.thoughtworks.model.Event;
import com.thoughtworks.schema.EventDeserializationSchema;
import com.thoughtworks.service.UserProfileService;
import com.thoughtworks.utils.MapUtils;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.elasticsearch.ElasticsearchSinkFunction;
import org.apache.flink.streaming.connectors.elasticsearch.RequestIndexer;
import org.apache.flink.streaming.connectors.elasticsearch6.ElasticsearchSink;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.http.HttpHost;
import org.elasticsearch.action.update.UpdateRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Application {

    private static final ApplicationPropertiesReader PROPERTIES_READER =
            ApplicationPropertiesReader.getInstance();

    public static void main(String[] args) throws Exception {
        run();
    }

    private static void run() throws Exception {
        String kafkaNodes = PROPERTIES_READER.read("kafka.nodes");
        String kafkaTopic = PROPERTIES_READER.read("kafka.topic");
        String kafkaGroup = PROPERTIES_READER.read("kafka.group");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        FlinkKafkaConsumer<Event> flinkKafkaConsumer = createEventConsumerForTopic(kafkaTopic, kafkaNodes, kafkaGroup);
        flinkKafkaConsumer.setStartFromLatest();

        DataStream<Event> stream = env
                .addSource(flinkKafkaConsumer)
                .map(s -> s);
        stream.print();

        ElasticsearchSink.Builder<Event> esSinkBuilder = newElasticsearchSinkBuilder();
        stream.addSink(esSinkBuilder.build());

        env.execute("flink response event");
    }

    @SuppressWarnings("unchecked")
    private static ElasticsearchSink.Builder<Event> newElasticsearchSinkBuilder() {
        String esSchema = PROPERTIES_READER.read("es.schema");
        String esNodes = PROPERTIES_READER.read("es.nodes");
        String esPort = PROPERTIES_READER.read("es.port");
        String esIndex = PROPERTIES_READER.read("userprofile.index");
        String esType = PROPERTIES_READER.read("userprofile.type");

        List<HttpHost> httpHosts = new ArrayList<>();
        httpHosts.add(new HttpHost(esNodes, Integer.valueOf(esPort), esSchema));
        ElasticsearchSink.Builder<Event> esSinkBuilder =
                new ElasticsearchSink.Builder<>(httpHosts, new ElasticsearchSinkFunction<Event>() {
                    UpdateRequest updateIndexRequest(Event event) throws IOException {
                        System.out.println("event: " + event.toString());
                        Map<String, Object> record = UserProfileService.responseEvent(event);
                        String id = (String) MapUtils.get(record, "_id").get();
                        Map<String, Object> content = (Map<String, Object>) MapUtils.get(record, "_source").get();

                        return new UpdateRequest().index(esIndex).type(esType).id(id).doc(content);
                    }

                    @Override
                    public void process(Event element, RuntimeContext ctx, RequestIndexer indexer) {
                        try {
                            indexer.add(updateIndexRequest(element));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        esSinkBuilder.setBulkFlushMaxActions(1);
        esSinkBuilder.setBulkFlushMaxSizeMb(500);
        esSinkBuilder.setBulkFlushInterval(5000);

        return esSinkBuilder;
    }

    private static FlinkKafkaConsumer<Event> createEventConsumerForTopic(String topic,
                                                                         String kafkaAddress,
                                                                         String kafkaGroup) {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", kafkaAddress);
        properties.setProperty("group.id", kafkaGroup);

        return new FlinkKafkaConsumer<>(topic, new EventDeserializationSchema(), properties);
    }

}
