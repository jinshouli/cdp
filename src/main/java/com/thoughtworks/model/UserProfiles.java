package com.thoughtworks.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfiles {

    private String customerId;
    private Integer age;
    private Integer genderCode;
    private String gender;
    private Integer drivingYears;
    private Integer carInfoCode;
    private String carDescription;
    private Double carPrice;
    private Integer carIndex;
    private Integer bmwCarIndex;

    private Integer dailyMilestone;
    private Integer lastMaintainMilestone;
    private Integer maintainTimes;
    private Integer maintainInterval;
    private Integer days;
    private Double latestMutationCoefficient;
    private String carContractTimeCode;
    private LocalDate carContractTime;
    private LocalDate pickCarTime;
    private Double carAge;

    private Integer firstDateDate;
    private LocalDate firstBuyCarTime;
    private Integer lifeValue;
    private Integer topOneBrandCode;
    private String topOneBrand;
    private String topFiveFrequencyVocabulary;
    private String topOneActiveCity;
    private String topOneActivity;
    private Boolean isLoad;
    private Integer loanRepayRemainMonths;

    private Double carInsuranceFee;
    private Double getCarInsuranceRate;
    private String carBelongingCity;
    private Double carAccessoryFee;
    private String carAccessoryCode;
    private String topOneCarAccessoryItem;
    private Double topOneProductPrice;
    private String topTwoCarAccessoryItem;
    private Double topTwoProductPrice;
    private Double carAccessorySelfRate;

    private Double carAccessoryGroupRate;
    private Double totalConsumption;
    private Double totalProfit;

    public static UserProfiles build(String csvRow) {
        String[] orgCells = csvRow.split(",");
        String[] cells = new String[43];
        for (int i = 0; i < 43; ++i) {
            if (i < orgCells.length) {
                cells[i] = orgCells[i];
            }
        }

        return new UserProfiles(
                toString(cells[0]),
                toInteger(cells[1]),
                toInteger(cells[2]),
                toString(cells[3]),
                toInteger(cells[4]),
                toInteger(cells[5]),
                toString(cells[6]),
                toDouble(cells[7]),
                toInteger(cells[8]),
                toInteger(cells[9]),

                toInteger(cells[10]),
                toInteger(cells[11]),
                toInteger(cells[12]),
                toInteger(cells[13]),
                toInteger(cells[14]),
                toDouble(cells[15]),
                toString(cells[16]),
                toLocalDate(cells[17]),
                toLocalDate(cells[18]),
                toDouble(cells[19]),

                toInteger(cells[20]),
                toLocalDate(cells[21]),
                toInteger(cells[22]),
                toInteger(cells[23]),
                toString(cells[24]),
                toString(cells[25]),
                toString(cells[26]),
                toString(cells[27]),
                toBoolean(cells[28]),
                toInteger(cells[29]),

                toDouble(cells[30]),
                toDouble(cells[31]),
                toString(cells[32]),
                toDouble(cells[33]),
                toString(cells[34]),
                toString(cells[35]),
                toDouble(cells[36]),
                toString(cells[37]),
                toDouble(cells[38]),
                toDouble(cells[39]),

                toDouble(cells[40]),
                toDouble(cells[41]),
                toDouble(cells[42]));
    }

    private static Integer toInteger(String cellValue) {
        if (null == cellValue || cellValue.trim().isEmpty() || cellValue.equals("#N/A")) {
            return null;
        }
        return Integer.valueOf(cellValue);
    }

    private static Double toDouble(String cellValue) {
        if (null == cellValue || cellValue.trim().isEmpty() || cellValue.equals("#N/A")) {
            return null;
        }
        return Double.valueOf(cellValue);
    }

    private static LocalDate toLocalDate(String cellValue) {
        if (null == cellValue || cellValue.trim().isEmpty() || cellValue.equals("#N/A")) {
            return null;
        }
        return LocalDate.parse(cellValue);
    }

    private static String toString(String cellValue) {
        if (null == cellValue || cellValue.trim().isEmpty() || cellValue.equals("#N/A")) {
            return null;
        }
        return cellValue;
    }

    private static Boolean toBoolean(String cellValue) {
        if (null == cellValue || cellValue.trim().isEmpty() || cellValue.equals("#N/A")) {
            return null;
        }
        return cellValue.equals("是");
    }

}
