package com.thoughtworks.model;

import lombok.Data;

import java.util.Map;

@Data
public class UpdateDailyMilestoneEventPayload {

    private String customerId;
    private String dailyMilestone;

    public UpdateDailyMilestoneEventPayload(Map<String, Object> map) {
        this.customerId = (String) map.get("customerId");
        this.dailyMilestone = (String)map.get("dailyMilestone");
    }
}
