package com.thoughtworks.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thoughtworks.enums.EventType;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Event {

    private EventType type;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date occurredTime;

    private Object payload;

}
